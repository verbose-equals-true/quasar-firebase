describe("Create Books", () => {
  it("can create a book document", () => {
    cy.visit("/books");
    cy.get(".books").then($books => {
      const bookCount = $books.children().length;
      cy.get("#add").click();
      cy.get(".books").then($newBooks => {
        expect($newBooks.children().length).to.equal(bookCount + 1);
      });
    });
  });
});
