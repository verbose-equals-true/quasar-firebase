# Quasar Firebase Template

## docker-compose quickstart

To start the app locally, first copy `.env.template` to `.env` and add your `FIREBASE_PROJECT_ID`. Then, run:

```bash
docker-compose up
```

## Deployment to GitLab pages

The repo is set to deploy a progressive web application to GitLab pages.

Before this is done, add the following environment variable to Settings > CI/CD:

```bash
FIREBASE_PROJECT_ID
```

## To Do

- [x] Write documents to Firestore collection
- [x] Read documents from Firestore collection
- [x] Install Firebase Tools
- [x] Deploy rules
- [x] Deploy rules from GitLab CI
- [x] Create and install Firebase app
- [x] Login/Sign up users by email
- [x] Create books
- [ ] Update books
- [x] Read books
- [x] Delete books
- [x] Install Cypress, set up a simple test
- [ ] Setup a Cypress tests in GitLab CI
- [ ] Display books in table
- [ ] Add pagination to table example
- [ ] Add CRUD actions to table expansion items
- [ ] Add documentation site with GitLab Pages

## Issues

I'm running into the issue described here: [https://github.com/vuejs/vuex/issues/1399](https://github.com/vuejs/vuex/issues/1399)

Using [this `firebase` boot file](https://github.com/kpapro/quasar-firebase-firestore/blob/master/src/boot/firebase.js), the database is defined on the Vue instance:

```js
    Vue.prototype.$fb = firebase
    Vue.prototype.$db = firestore
```

[Vuexfire documentation](https://vuefire.vuejs.org/vuexfire/getting-started.html#easy-access-to-firebase-database) suggests that we define `db.js` and import this anywhere as needed. There are errors when we call `initializeApp` more than once.

Currently, I'm passing `this` along with the actions used for binding collections to the store as described [here](https://github.com/vuejs/vuex/issues/1399). This works, but there is a probably a better way to do this.

## `firebase init` for an existing project

Since we already have an existing project in Firebase, use the `--project` flag and pass the Firebase project ID:

```bash
firebase init --project quasar-firebase-abc123

     ######## #### ########  ######## ########     ###     ######  ########
     ##        ##  ##     ## ##       ##     ##  ##   ##  ##       ##
     ######    ##  ########  ######   ########  #########  ######  ######
     ##        ##  ##    ##  ##       ##     ## ##     ##       ## ##
     ##       #### ##     ## ######## ########  ##     ##  ######  ########

You're about to initialize a Firebase project in this directory:

  /home/brian/gitlab/quasar-firebase-base/firebase

? Which Firebase CLI features do you want to set up for this folder? Press Space to select features,
then Enter to confirm your choices. (Press <space> to select, <a> to toggle all, <i> to invert select
ion)
❯◯ Database: Deploy Firebase Realtime Database Rules
 ◯ Firestore: Deploy rules and create indexes for Firestore
 ◯ Functions: Configure and deploy Cloud Functions
 ◯ Hosting: Configure and deploy Firebase Hosting sites
 ◯ Storage: Deploy Cloud Storage security rules
```

This will prompt us to make some selections about the Firebase CLI features we want to use.

## Examples

```js
export default new Vuex.Store({
  state: {
    data: {},
  },
  actions: {
    bindById: firebaseAction(async ({bindFirebaseRef, state}, id) => {
      // prevent double binding...
      if (state.data[id]) {
        return;
      }
      const couplingRef = couplingsRef.doc(id);
      await bindFirebaseRef(`data.${id}`, couplingRef);
    }),
  },
});
```

## `firebase deploy`

```bash
 firebase deploy

=== Deploying to 'quasar-firebase-abc123'...

i  deploying firestore, functions
Running command: npm --prefix "$RESOURCE_DIR" run lint

> functions@ lint /home/brian/gitlab/quasar-firebase-base/firebase/functions
> eslint .

✔  functions: Finished running predeploy script.
i  firestore: checking firestore.rules for compilation errors...
i  firestore: reading indexes from firestore.indexes.json...
✔  firestore: rules file firestore.rules compiled successfully
i  functions: ensuring necessary APIs are enabled...
✔  functions: all necessary APIs are enabled
i  firestore: uploading rules firestore.rules...
✔  firestore: deployed indexes in firestore.indexes.json successfully
i  functions: preparing functions directory for uploading...
✔  firestore: released rules firestore.rules to cloud.firestore

✔  Deploy complete!

Project Console: https://console.firebase.google.com/project/quasar-firebase-abc123/overview
```

## Deploy from GitLab CI

Run the following command locally:

```bash
firebase login:ci
```

This will prompt you to authenticate, and then it will give you a token that looks like this:

```bash
1/IEpQooaI_OIrkroKjjoKirocKckrdlr3N9FLet30DyGq
d6S
```

Save this token in GitLab CI with the variable name `FIREBASE_TOKEN`. We can use this variable in GitLab CI like this:

```bash
firebase deploy --token "$FIREBASE_TOKEN"
```

## Authentication

To use authentication, you first need to enable the specific type of authentication provider in the Firebase console. Let's start with email.

We have defined two authentication-related methods on the Vue instance in the `firebase.js` boot file. We can use these in our authentication modal by simply setting

```html
<q-form @submit="register">
```

and then defining `register` to be

```js
    register() {
      this.$registerUser(this.email, this.password).then(() => {
        this.$q.notify({
          title: "User registered",
          message: "Thank you for registering!"
        });
      });
    },
```

We can verify that the user has been created successfully by checking the Firebase console for the email we submitted, and we can also verify that the user is currently logged in by opening the console and going to IndexedDB > firebaseLocalStorageDb > firebaseLocalStorage. We should see the following key/value:

```json
{
    fbase_key: "firebase:authUser:AIzaSyBSGxeSMo5Mgb2kIgRK5f9TOuK4iI09_VM:[DEFAULT]",
    value: {
        apiKey: "AIzaSyBSGxeSMo5Mgb2kIgRK5f9TOuK4iI09_VM",
        appName: "[DEFAULT]",
        authDomain: "quasar-firebase-423dc.firebaseapp.com",
        createdAt: "1567256474989",
        displayName: null,
        email: "brian12345@gmail.com",
        emailVerified: false,
        isAnonymous: false,
        lastLoginAt: "1567256474989",
        phoneNumber: null,
        photoURL: null,
        providerData:{
            displayName: null,
            email: "brian12345@gmail.com",
            phoneNumber: null,
            photoURL: null,
            providerId: "password",
            uid: "brian12345@gmail.com"
        },
        length: 1,
        redirectEventId: null,
        stsTokenManager:,
        accessToken: "eyJhbGciOiJSUzI1NiIsImtpZCI6IIJJOI098787IHuiHIU989UYUWZhYmQ4MTIyNTRiMWJmOTE4YzAiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vcXVhc2FyLWZpcmViYXNlLTQyM2RjIiwiYXVkIjoicXVhc2FyLWZpcmViYXNlLTQyM2RjIiwiYXV0aF90aW1lIjoxNTY3MjU2NDc1LCJ1c2VyX2lkIjoibTViWGNHdmpYYVBsT1l3ajhvSzVKcDV0VUJ0MiIsInN1YiI6Im01YlJHBJHBJHB8989IUbE9Zd2o4b0s1SnA1dFVCdDIiLCJpYXQiOjE1NjcyNTY0NzUsImV4cCI6MTU2NzI2MDA3NSwiZW1haWwiOiJicmlhbjEyMzQ1QGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJicmlhbjEyMzQ1QGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.Dc4Xa9I39maccQCucAXk-bx5CYrFMlce2cV8T2EldX1RI1CqCZ-D6zrYNL6v10ECiEjdotMZVvJwytXlJhHZsKHWqvxOBokqOcukvzIcBntoK53jn7oixhVOw2WypjgqIB8SATcUG-AMDl-uakCnO7qzgAERii3zm93cHO838Pi3DjMLPAAf0lTSOAlpfGCXyATHpYX4zfECjkhxki3ujfknr877efknjre787frekj98fnrfrnqD5qwV8BO9M8GjdwQeFWQzwc8R_6v0LUAAa5dphjHSo1y-xVdgImjzGAlaTgSQVliDGNAssHwVk8ZuiFZtep__iWPce7TK0M42FJFrp7lZ6ug",
        apiKey: "AIzaSyBSGxeSMo5MKRKFR98K5f9TOuK4iI09_VM",
        expirationTime: 1567260075670,
        refreshToken: "AEu4IL3TDhCITY8UUUEBvT-hPBiG30iBC5gyn2AbX0SFU5FReEvZXgRYwRby4_O5wjeBh2RgOQbeFPJGsG_RcTKUc7L30oEFR4sCzkdLXWfOMs7UMdAnzaXG48iufi3498iuIUIUHRFgiAYJ1EIqEQy68gGYVTYzmfc3t5BjABZLSbk4BctxqHp4UEdkJQgXyiYtqfhKhrpx8DR9xiIfjHldjdOMu4_5OgbBKN-EkWCXd2fKKO2wqM15N-w",
        uid: "m5bXc(9r8frfcfkJCIUFj8oK5Jp5tUBt2"
    }
```

Using authentication state to conditionally display information in our application.

Next let's use our authentication state to conditionally display links in our application. We will also want to setup route guards.

## Cypress for e2e and integration testing

Install Cypress in the root of the project

```bash
npm install cypress --save-dev
```

Add `node_modules/` to `.gitignore`

Run the following command to start Cypress:

```bash
$(npm bin)/cypress open
```
