const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      { path: "", component: () => import("pages/Index.vue") },
      { path: "login", component: () => import("pages/Login.vue") },
      { path: "signup", component: () => import("pages/SignUp.vue") },
      {
        path: "books",
        component: () => import("pages/Books.vue")
      },
      { path: "books/:id", component: () => import("pages/BookDetail.vue") },
      { path: "settings", component: () => import("pages/Settings.vue") },
      {
        path: "profile",
        component: () => import("pages/Profile.vue"),
        meta: {
          requiresAuth: true
        }
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
