const state = {
  visible: false,
  darkMode: false,
  leftDrawerOpen: false,
  nextLink: null,
  isDark: true,
  authPanel: "login"
};

const getters = {
  leftDrawerOpen: s => s.leftDrawerOpen,
  authModalVisible: s => s.visible,
  getNextLink: s => s.nextLink,
  isDark: s => s.isDark,
  getAuthPanel: s => s.authPanel
};

const mutations = {
  setAuthPanel: (state, payload) => {
    state.authPanel = payload;
  },
  toggleLoginMenu: state => {
    state.visible = !state.visible;
  },
  toggleDarkMode: state => {
    state.isDark = !state.isDark;
  },
  toggleLeftDrawer: (state, payload) => {
    if (payload) {
      state.leftDrawerOpen = payload.leftDrawerOpen;
      return;
    }
    state.leftDrawerOpen = !state.leftDrawerOpen;
  },
  setNextLink: (state, payload) => {
    state.nextLink = payload.nextLink;
  }
};

export default {
  state,
  getters,
  mutations
};
