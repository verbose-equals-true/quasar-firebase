import { firestoreAction } from "vuexfire";

const state = {
  books: []
};
const getters = {
  count: state => state.books.length
};
const actions = {
  bindBooksRef: firestoreAction((context, { vm }) => {
    return context.bindFirestoreRef(
      "books",
      vm.$db.collection("books").orderBy("timestamp", "desc")
    );
  }),
  unbindBooksRef: firestoreAction(context => {
    return context.unbindFirestoreRef("books");
  })
};

export default {
  state,
  getters,
  actions
};
