import { vuexfireMutations } from "vuexfire";
import Vue from "vue";
import Vuex from "vuex";
import ui from "./ui";
import books from "./books";
// import { db } from "../utils/db";

// import example from './module-example'

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      ui,
      books
    },
    mutations: { ...vuexfireMutations },
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
