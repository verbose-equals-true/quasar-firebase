import * as firebase from "firebase";

// Get a Firestore instance
export const db = firebase
  .initializeApp({ projectId: process.env.projectId })
  .firestore();

// Export types that exists in Firestore
// This is not always necessary, but it's used in other examples
const { TimeStamp, GeoPoint } = firebase.firestore;
export { TimeStamp, GeoPoint };
