import BasePage from "components/ui/BasePage.vue";
import LeftMenuLink from "components/ui/LeftMenuLink.vue";
import MainLeftDrawer from "components/ui/MainLeftDrawer.vue";
import MainHeader from "components/ui/MainHeader.vue";
import PageHeader from "components/ui/PageHeader.vue";
import PageSubHeader from "components/ui/PageSubHeader.vue";
import PageText from "components/ui/PageText.vue";
import BaseBtn from "components/ui/BaseBtn.vue";
import BaseCard from "components/ui/BaseCard.vue";
import Book from "components/Book.vue";
import BasePre from "components/ui/BasePre.vue";

// leave the export, even if you don't use it
export default async ({ Vue }) => {
  Vue.component("BasePage", BasePage);
  Vue.component("LeftMenuLink", LeftMenuLink);
  Vue.component("MainLeftDrawer", MainLeftDrawer);
  Vue.component("MainHeader", MainHeader);
  Vue.component("PageHeader", PageHeader);
  Vue.component("PageSubHeader", PageSubHeader);
  Vue.component("PageText", PageText);
  Vue.component("BaseBtn", BaseBtn);
  Vue.component("BaseCard", BaseCard);
  Vue.component("Book", Book);
  Vue.component("BasePre", BasePre);
};
