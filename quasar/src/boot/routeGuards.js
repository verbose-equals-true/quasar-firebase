import firebase from "firebase/app";
// require("firebase/auth");

export default ({ router }) => {
  router.beforeEach((to, from, next) => {
    firebase.auth().onAuthStateChanged(() => {
      const currentUser = firebase.auth().currentUser;
      const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
      if (requiresAuth && !currentUser) {
        next("login");
      } else if (to.path === "/login" && (!requiresAuth && currentUser)) {
        next("profile");
      } else if (to.path === "/signup" && (!requiresAuth && currentUser)) {
        next("profile");
      } else {
        next();
      }
    });
  });
};
