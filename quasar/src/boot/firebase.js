/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

// import * as configs from '../utils/firebase/configs.js'
import firebase from "firebase/app";
import "firebase/firestore";
// import { firestorePlugin } from "vuefire";
require("firebase/auth");

// Initialize app
const currentConfig = {
  apiKey: process.env.apiKey,
  authDomain: process.env.authDomain,
  databaseURL: process.env.databaseURL,
  projectId: process.env.projectId,
  storageBucket: process.env.storageBucket,
  messagingSenderId: process.env.messagingSenderId,
  appId: process.env.appId
};

export default ({ app, router, Vue }) => {
  // Vue.use(firestorePlugin);

  console.log("PROCESS ENV OBJECT", process.env);
  // Make sure the firebase keys have been set accordingly

  if (currentConfig) {
    // if (!firebase.apps.length) {
    firebase.initializeApp(currentConfig);
    // }
    // Initialize Cloud Firestore through Firebase
    const firestore = firebase.firestore();

    // Add props to our Vue instance for easy access
    // in our app
    Vue.prototype.$fb = firebase;
    Vue.prototype.$db = firestore;
  }

  // Add auth methods to our Vue instance
  Vue.prototype.$login = (email, password) => {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(user => {
          resolve(user);
        })
        .catch(error => {
          reject(error);
        });
    });
  };

  Vue.prototype.$registerUser = (email, password) => {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(user => {
          resolve(user);
        })
        .catch(error => {
          reject(error);
        });
    });
  };
};
